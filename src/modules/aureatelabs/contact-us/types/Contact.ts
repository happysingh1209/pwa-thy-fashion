export default interface Contact {
  about: string[],
  name: string,
  email: string,
  comment: string
}
