import { GetterTree } from 'vuex'
import RootState from '@vue-storefront/core/types/RootState'
import AuthState from '../types/AuthState'

const getters: GetterTree<AuthState, RootState> = {
  getLoginUserInfo: state => state.loginUser
}

export default getters
